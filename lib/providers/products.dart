import 'package:flutter/material.dart';
import '../models/product.dart';

class Products with ChangeNotifier {
  List<Product> _items = [
    Product(
        id: 'p1',
        title: 'Red shirt',
        description: 'A red shirt',
        price: 12,
        imageUrl:
            'https://imagescdn.planetfashion.in/img/app/product/8/85181-250173.jpg?auto=format'),
    Product(
        id: 'p2',
        title: 'White shirt',
        description: 'A white shirt',
        price: 24,
        imageUrl:
            'https://cdn.shopify.com/s/files/1/0470/0389/3911/products/43_1024x1024.jpg?v=1671787976'),
    Product(
        id: 'p3',
        title: 'Blue shirt',
        description: 'A blue shirt',
        price: 22,
        imageUrl:
            'https://rukminim1.flixcart.com/image/832/832/xif0q/shirt/p/j/o/s-fo-shrt-499-nav-5th-anfold-original-imafxamjsk2p7ymc-bb.jpeg?q=70'),
    Product(
        id: 'p4',
        title: 'Yellow shirt',
        description: 'A yellow shirt',
        price: 18,
        imageUrl:
            'https://imagescdn.allensolly.com/img/app/product/5/529418-4013389.jpg?auto=format')
  ];

  List<Product> get items {
    return [..._items];
  }

  void addProduct() {
    // _items.add(value);
    notifyListeners();
  }
}
